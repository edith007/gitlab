<script>
import { GlEmptyState } from '@gitlab/ui';
import { debounce } from 'lodash';
import { setUrlFragment, queryToObject } from '~/lib/utils/url_utility';
import { s__, __ } from '~/locale';
import glFeatureFlagsMixin from '~/vue_shared/mixins/gl_feature_flags_mixin';
import { DEFAULT_DEBOUNCE_AND_THROTTLE_MS } from '~/lib/utils/constants';
import { POLICY_TYPE_COMPONENT_OPTIONS } from 'ee/security_orchestration/components/constants';
import { extractPolicyContent } from 'ee/security_orchestration/components/utils';
import {
  ACTIONS_LABEL,
  EDITOR_MODE_RULE,
  EDITOR_MODE_YAML,
  PARSING_ERROR_MESSAGE,
  SECURITY_POLICY_ACTIONS,
} from '../constants';
import { doesFileExist, getMergeRequestConfig, policyBodyToYaml, policyToYaml } from '../utils';
import EditorLayout from '../editor_layout.vue';
import DimDisableContainer from '../dim_disable_container.vue';
import ActionSection from './action/action_section.vue';
import RuleSection from './rule/rule_section.vue';
import { createPolicyObject, getInitialPolicy } from './utils';
import {
  CONDITIONS_LABEL,
  DEFAULT_PIPELINE_EXECUTION_POLICY,
  DEFAULT_PIPELINE_EXECUTION_POLICY_NEW_FORMAT,
} from './constants';

export default {
  ACTION: 'actions',
  EDITOR_MODE_RULE,
  EDITOR_MODE_YAML,
  SECURITY_POLICY_ACTIONS,
  i18n: {
    ACTIONS_LABEL,
    CONDITIONS_LABEL,
    PARSING_ERROR_MESSAGE,
    notOwnerButtonText: __('Learn more'),
    createMergeRequest: s__('SecurityOrchestration|Update via merge request'),
  },
  components: {
    ActionSection,
    DimDisableContainer,
    GlEmptyState,
    EditorLayout,
    RuleSection,
  },
  mixins: [glFeatureFlagsMixin()],
  inject: [
    'disableScanPolicyUpdate',
    'namespacePath',
    'policyEditorEmptyStateSvgPath',
    'scanPolicyDocumentationPath',
  ],
  props: {
    assignedPolicyProject: {
      type: Object,
      required: true,
    },
    existingPolicy: {
      type: Object,
      required: false,
      default: null,
    },
    isCreating: {
      type: Boolean,
      required: true,
    },
    isDeleting: {
      type: Boolean,
      required: true,
    },
    isEditing: {
      type: Boolean,
      required: true,
    },
  },
  data() {
    let yamlEditorValue;

    if (this.existingPolicy) {
      yamlEditorValue = policyToYaml(
        this.existingPolicy,
        POLICY_TYPE_COMPONENT_OPTIONS.pipelineExecution.urlParameter,
      );
    } else {
      const manifest = this.glFeatures.securityPoliciesNewYamlFormat
        ? DEFAULT_PIPELINE_EXECUTION_POLICY_NEW_FORMAT
        : DEFAULT_PIPELINE_EXECUTION_POLICY;

      yamlEditorValue = getInitialPolicy(manifest, queryToObject(window.location.search));
    }

    const { policy, hasParsingError } = createPolicyObject(yamlEditorValue);
    const parsingError = hasParsingError ? this.$options.i18n.PARSING_ERROR_MESSAGE : '';

    return {
      documentationPath: setUrlFragment(
        this.scanPolicyDocumentationPath,
        'pipeline-execution-policy-editor',
      ),
      hasParsingError,
      disableSubmit: false,
      mode: EDITOR_MODE_RULE,
      parsingError,
      policy,
      yamlEditorValue,
    };
  },
  computed: {
    originalName() {
      return this.existingPolicy?.name;
    },
    strategy() {
      return this.policy.pipeline_config_strategy;
    },
    content() {
      return this.policy?.content;
    },
  },
  watch: {
    content(newVal) {
      this.handleFileValidation(newVal);
    },
  },
  mounted() {
    if (this.existingPolicy) {
      this.handleFileValidation(this.existingPolicy?.content);
    }
  },
  created() {
    this.handleFileValidation = debounce(this.doesFileExist, DEFAULT_DEBOUNCE_AND_THROTTLE_MS);
  },
  destroyed() {
    this.handleFileValidation.cancel();
  },
  methods: {
    changeEditorMode(mode) {
      this.mode = mode;
    },
    async handleModifyPolicy(action) {
      const extraMergeRequestInput = getMergeRequestConfig(queryToObject(window.location.search), {
        namespacePath: this.namespacePath,
      });

      /**
       * backend only accepts the old format
       * policy body is extracted
       * and policy type is added to a policy body
       */
      const type = POLICY_TYPE_COMPONENT_OPTIONS.pipelineExecution.urlParameter;
      const policy = extractPolicyContent({ manifest: this.yamlEditorValue, type, withType: true });

      const payload = this.glFeatures.securityPoliciesNewYamlFormat
        ? policyBodyToYaml(policy)
        : this.yamlEditorValue;

      this.$emit('save', { action, extraMergeRequestInput, policy: payload });
    },
    async doesFileExist(value) {
      const { project, ref = null, file } = value?.include?.[0] || {};

      try {
        const exists = await doesFileExist({
          fullPath: project,
          filePath: file,
          ref,
        });

        this.disableSubmit = !exists;
      } catch {
        this.disableSubmit = true;
      }
    },
    handleUpdateProperty(property, value) {
      this.policy[property] = value;
      this.updateYamlEditorValue(this.policy);
    },
    handleUpdateYaml(manifest) {
      const { policy, hasParsingError } = createPolicyObject(manifest);

      this.yamlEditorValue = manifest;
      this.hasParsingError = hasParsingError;
      this.parsingError = hasParsingError ? this.$options.i18n.PARSING_ERROR_MESSAGE : '';
      this.policy = policy;
    },
    updateYamlEditorValue(policy) {
      this.yamlEditorValue = policyToYaml(
        policy,
        POLICY_TYPE_COMPONENT_OPTIONS.pipelineExecution.urlParameter,
      );
    },
  },
};
</script>

<template>
  <editor-layout
    v-if="!disableScanPolicyUpdate"
    :custom-save-button-text="$options.i18n.createMergeRequest"
    :has-parsing-error="hasParsingError"
    :is-editing="isEditing"
    :is-removing-policy="isDeleting"
    :is-updating-policy="isCreating"
    :parsing-error="parsingError"
    :policy="policy"
    :yaml-editor-value="yamlEditorValue"
    @remove-policy="handleModifyPolicy($options.SECURITY_POLICY_ACTIONS.REMOVE)"
    @save-policy="handleModifyPolicy()"
    @update-editor-mode="changeEditorMode"
    @update-property="handleUpdateProperty"
    @update-yaml="handleUpdateYaml"
  >
    <template #rules>
      <dim-disable-container :disabled="hasParsingError">
        <template #title>
          <h4>{{ $options.i18n.CONDITIONS_LABEL }}</h4>
        </template>

        <template #disabled>
          <div class="gl-rounded-base gl-bg-gray-10 gl-p-6"></div>
        </template>

        <rule-section class="gl-mb-4" />
      </dim-disable-container>
    </template>

    <template #actions-first>
      <dim-disable-container data-testid="actions-section" :disabled="hasParsingError">
        <template #title>
          <h4>{{ $options.i18n.ACTIONS_LABEL }}</h4>
        </template>

        <template #disabled>
          <div class="gl-rounded-base gl-bg-gray-10 gl-p-6"></div>
        </template>

        <action-section
          class="security-policies-bg-subtle gl-mb-4 gl-rounded-base gl-p-5"
          :action="policy.content"
          :does-file-exist="!disableSubmit"
          :strategy="strategy"
          :suffix="policy.suffix"
          @changed="handleUpdateProperty"
        />
      </dim-disable-container>
    </template>
  </editor-layout>

  <gl-empty-state
    v-else
    :description="$options.i18n.notOwnerDescription"
    :primary-button-link="documentationPath"
    :primary-button-text="$options.i18n.notOwnerButtonText"
    :svg-path="policyEditorEmptyStateSvgPath"
    :svg-height="null"
    title=""
  />
</template>

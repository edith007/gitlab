# frozen_string_literal: true

module Admin
  module ApplicationSettingsHelper
    extend self

    delegate :duo_availability,
      :instance_level_ai_beta_features_enabled,
      to: :'Gitlab::CurrentSettings.current_application_settings'

    def ai_powered_testing_agreement
      safe_format(
        s_('AIPoweredSM|By enabling this feature, you agree to the %{link_start}GitLab Testing Agreement%{link_end}.'),
        tag_pair_for_link(gitlab_testing_agreement_url))
    end

    def ai_powered_description
      safe_format(
        s_('AIPoweredSM|Enable %{link_start}AI-powered features%{link_end} for this instance.'),
        tag_pair_for_link(ai_powered_docs_url))
    end

    def direct_connections_description
      safe_format(
        s_('AIPoweredSM|Disable %{link_start}direct connections%{link_end} for this instance.'),
        tag_pair_for_link(direct_connections_docs_url))
    end

    def admin_display_duo_addon_settings?
      CloudConnector::AvailableServices.find_by_name(:code_suggestions)&.purchased?
    end

    def admin_duo_home_app_data
      subscription_name = License.current.subscription_name
      {
        duo_seat_utilization_path: admin_gitlab_duo_seat_utilization_index_path,
        duo_configuration_path: admin_gitlab_duo_configuration_index_path,
        add_duo_pro_seats_url: add_duo_pro_seats_url(subscription_name),
        subscription_name: subscription_name,
        is_bulk_add_on_assignment_enabled: 'true',
        subscription_start_date: License.current.starts_at,
        subscription_end_date: License.current.expires_at,
        duo_availability: duo_availability,
        direct_code_suggestions_enabled: ::Gitlab::CurrentSettings.disabled_direct_code_suggestions.to_s,
        experiment_features_enabled: instance_level_ai_beta_features_enabled.to_s,
        self_hosted_models_enabled: ::Ai::TestingTermsAcceptance.has_accepted?.to_s
      }
    end

    def admin_ai_general_settings_helper_data
      {
        on_general_settings_page: 'true',
        redirect_path: general_admin_application_settings_path
      }.merge(ai_settings_helper_data)
    end

    def admin_ai_configuration_settings_helper_data
      {
        on_general_settings_page: 'false',
        redirect_path: admin_gitlab_duo_path
      }.merge(ai_settings_helper_data)
    end

    def ai_settings_helper_data
      code_suggestions_purchased = CloudConnector::AvailableServices.find_by_name(:code_suggestions)&.purchased?
      disabled_direct_code_suggestions = ::Gitlab::CurrentSettings.disabled_direct_code_suggestions
      self_hosted_models_enabled = ::Ai::TestingTermsAcceptance.has_accepted?

      {
        duo_availability: duo_availability.to_s,
        experiment_features_enabled: instance_level_ai_beta_features_enabled.to_s,
        are_experiment_settings_allowed: "true",
        duo_pro_visible: code_suggestions_purchased.to_s,
        disabled_direct_connection_method: disabled_direct_code_suggestions.to_s,
        self_hosted_models_enabled: self_hosted_models_enabled.to_s,
        ai_terms_and_conditions_path: admin_ai_terms_and_conditions_path
      }
    end

    private

    # rubocop:disable Gitlab/DocUrl
    # We want to link SaaS docs for flexibility for every URL related to Code Suggestions on Self Managed.
    # We expect to update docs often during the Beta and we want to point user to the most up to date information.
    def ai_powered_docs_url
      'https://docs.gitlab.com/ee/user/ai_features.html'
    end

    def gitlab_testing_agreement_url
      'https://about.gitlab.com/handbook/legal/testing-agreement/'
    end

    def direct_connections_docs_url
      'https://docs.gitlab.com/ee/user/project/repository/code_suggestions/index.html#disable-direct-connections-to-the-ai-gateway'
    end
    # rubocop:enable Gitlab/DocUrl

    def tag_pair_for_link(url)
      tag_pair(link_to('', url, target: '_blank', rel: 'noopener noreferrer'), :link_start, :link_end)
    end
  end
end
